data "aws_iam_policy_document" "lambda" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
  }
}

resource "aws_iam_role" "lambda" {
  assume_role_policy = data.aws_iam_policy_document.lambda.json
  name               = "${var.stack-name}-lambda"
  tags = {
    Name = "${var.stack-name}-lambda"
  }
}

resource "aws_iam_role_policy" "lambda-policy" {
  name   = "lambda-policy"
  policy = templatefile("${path.module}/templates/lambda-policy.json.tpl",
    {
      s3-bucket-arn      = "arn:aws:s3:::${var.bucket-name}"
      dynamodb-table-arn = aws_dynamodb_table.main.arn
      log-group-arn      = aws_cloudwatch_log_group.lambda.arn
    }
  )
  role   = aws_iam_role.lambda.id
}
