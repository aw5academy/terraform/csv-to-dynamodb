resource "aws_dynamodb_table" "main" {
  billing_mode   = "PAY_PER_REQUEST"
  name           = var.stack-name
  hash_key       = "uuid"
  attribute {
    name = "uuid"
    type = "S"
  }
  tags = {
    Name = var.stack-name
  }
}
