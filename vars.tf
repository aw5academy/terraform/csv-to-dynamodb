variable "stack-name" {
  description = "The stack identifier."
  default     = "csv-to-dynamodb"
}

variable "bucket-name" {
  description = "Object creation events in this bucket will be sent to the SQS queue for virus scanning."
}
