# csv-to-dynamodb
A Terraform project to provision AWS resources which can load the records from a CSV file into DynamoDB.

This project uses the guide and sample code from [this AWS blog post](https://aws.amazon.com/blogs/database/implementing-bulk-csv-ingestion-to-amazon-dynamodb/).
