data "archive_file" "lambda" {
  output_path = ".lambda/csv-to-dynamodb.zip"
  source_dir  = "lambda-src/csv-to-dynamodb"
  type        = "zip"
}

resource "aws_lambda_function" "csv-to-dynamodb" {
  environment {
    variables = {
      DYNAMODB_TABLE_NAME = aws_dynamodb_table.main.name
    }
  }
  filename         = ".lambda/csv-to-dynamodb.zip"
  function_name    = var.stack-name
  handler          = "lambda_function.lambda_handler"
  role             = aws_iam_role.lambda.arn
  runtime          = "python3.7"
  source_code_hash = data.archive_file.lambda.output_base64sha256
  tags = {
    Name = var.stack-name
  }
  timeout = "900"
}
