resource "aws_cloudwatch_event_rule" "s3-object-tag" {
  is_enabled  = "true"
  name        = "${var.stack-name}-s3-object-tag"
  description = "Triggers the csv-to-dynamodb lambda when S3 objects are tagged."
  event_pattern = templatefile("${path.module}/templates/event-bridge.json.tpl",
    {
      s3-bucket-name = var.bucket-name
    }
  )
  tags = {
    Name = "${var.stack-name}-s3-object-tag"
  }
}

resource "aws_cloudwatch_event_target" "s3-object-tag" {
  arn  = aws_lambda_function.csv-to-dynamodb.arn
  rule = aws_cloudwatch_event_rule.s3-object-tag.name
}

resource "aws_lambda_permission" "s3-object-tag" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.csv-to-dynamodb.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.s3-object-tag.arn
  statement_id  = "AllowExecutionFromCloudWatch"
}
