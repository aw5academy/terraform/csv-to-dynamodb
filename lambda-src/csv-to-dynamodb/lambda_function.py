import json
import boto3
import os
import csv
import codecs
import sys

s3 = boto3.resource('s3')
s3_client = boto3.client('s3')
dynamodb = boto3.resource('dynamodb')
tableName = os.environ['DYNAMODB_TABLE_NAME']

def lambda_handler(event, context):  
  bucket = event["detail"]["requestParameters"]["bucketName"]
  key = event["detail"]["requestParameters"]["key"]
  if "x-amz-version-id" in event["detail"]["responseElements"]:
    obj_tags = s3_client.get_object_tagging(
      Bucket=bucket,
      Key=key,
      VersionId=event["detail"]["responseElements"]["x-amz-version-id"],
    )
  else:
    obj_tags = s3_client.get_object_tagging(
      Bucket=bucket,
      Key=key,
    )
  for obj_tag in obj_tags["TagSet"]:
    if obj_tag["Key"] == "av-status":
      AV_STATUS = obj_tag["Value"]
  if AV_STATUS == "CLEAN":
    csv_to_dynamodb(bucket, key)

  return {
    'statusCode': 200,
    'body': json.dumps('Success')
  }

def csv_to_dynamodb(bucket, key):
  #get() does not store in memory
  try:
    obj = s3.Object(bucket, key).get()['Body']
  except:
    print("S3 Object could not be opened. Check environment variable. ")
  try:
    table = dynamodb.Table(tableName)
  except:
    print("Error loading DynamoDB table. Check if table was created correctly and environment variable.")

  batch_size = 100
  batch = []

  #DictReader is a generator; not stored in memory
  for row in csv.DictReader(codecs.getreader('utf-8')(obj)):
    if len(batch) >= batch_size:
      write_to_dynamo(batch)
      batch.clear()
    batch.append(row)

  if batch:
    write_to_dynamo(batch)

  return {
    'statusCode': 200,
    'body': json.dumps('Uploaded to DynamoDB Table')
  }

def write_to_dynamo(rows):
  try:
    table = dynamodb.Table(tableName)
  except:
    print("Error loading DynamoDB table. Check if table was created correctly and environment variable.")

  try:
    with table.batch_writer() as batch:
      for i in range(len(rows)):
        batch.put_item(
        Item=rows[i]
      )
  except:
    print("Error executing batch_writer")
