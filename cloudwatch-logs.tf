resource "aws_cloudwatch_log_group" "lambda" {
  name              = "/aws/lambda/${var.stack-name}"
  retention_in_days = "30"
  tags = {
    Name = "/aws/lambda/${var.stack-name}"
  }
}
